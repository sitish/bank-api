const { user, account, transaction } = require("../models"); // Import all models

// user and transaksi relationship
user.hasMany(account, { foreignKey: "user_id" });
account.belongsTo(user, { foreignKey: "user_id" });

// account and transaction relationship
account.hasMany(transaction, { foreignKey: "account_id" });
transaction.belongsTo(account, { foreignKey: "account_id" });


