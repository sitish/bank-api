**Tech Stack**

Tech stack that i used:

- Engine:Node Js
- Framework:Express js
- Database: Postgres (using Sequelize)
- Authentication & Authorization: JWT, Passport
- Unit Testing: Jest


## Install & Usage

Start by cloning this repository

```sh
# HTTPS
$ git clone https://gitlab.com/sitish/bank-api
```

then

```sh
# cd into project root
$ npm i
# run the api in development mode
$ npm  run dev

```
## Documentation

documentation created by Postman.
url documentation: https://documenter.getpostman.com/view/7578520/TzRNFVLx
