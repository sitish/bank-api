const passport = require("passport"); 
const LocalStrategy = require("passport-local").Strategy; 
const bcrypt = require("bcrypt"); 
const JWTstrategy = require("passport-jwt").Strategy; 
const ExtractJWT = require("passport-jwt").ExtractJwt; 
const { user } = require("../../models"); 

exports.signup = (req, res, next) => {
  passport.authenticate("signup", { session: false }, (err, user, info) => {

    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err.message,
      });
    }

    // If user is false
    if (!user) {
      return res.status(401).json({
        message: info.message,
      });
    }
    // And it will bring to controller
    req.user = user;

    // Next to authController.getToken
    next();
  })(req, res, next);
};

// If user call this passport
passport.use(
  "signup",
  new LocalStrategy(
    {
      usernameField: "email", // usernameField is come from req.body.email
      passwordField: "password", // passwordField is come from req.body.password
      passReqToCallback: true, // enable to read req.body/req.params/req.query
    },
    async (req, email, password, done) => {
      try {
          console.log(req.body,"cekk");
        let userSignUp = await user.create(req.body);

        return done(null, userSignUp, {
          message: "User can be created",
        });
      } catch (e) {
        console.log(e);
        return done(null, false, {
          message: "User can't be created",
        });
      }
    }
  )
);

exports.signin = (req, res, next) => {
  // It will go to ../middlewares/auth/index.js -> passport.use("signin")
  passport.authenticate("signin", { session: false }, (err, user, info) => {

    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err.message,
      });
    }

    // If user is false
    if (!user) {
      return res.status(401).json({
        message: info.message,
      });
    }

    // Make req.user that will be save the user value
    // And it will bring to controller
    console.log(user,"cekk di validator");
    req.user = user;

    // Next to authController.getToken
    next();
  })(req, res, next);
};

// If user call this passport
passport.use(
  "signin",
  new LocalStrategy(
    {
      usernameField: "email", // usernameField is come from req.body.email
      passwordField: "password", // passwordField is come from req.body.password
      passReqToCallback: true, // enable to read req.body/req.params/req.query
    },
    async (req, email, password, done) => {
      try {
        // After user call this passport
        // It will run this method and create the user depends on req.body
        let userSignIn = await user.findOne({ where:{email:email} });
        console.log(userSignIn);

        // If user doesn't exist
        if (!userSignIn) {
          return done(null, false, {
            message: "Email not found",
          });
        }
        // If user exist
        let validate = await bcrypt.compare(password, userSignIn.password);
        console.log(validate);

        // If password is wrong
        if (!validate) {
          return done(null, false, {
            message: "Wrong password",
          });
        }
        return done(null, userSignIn, {
          message: "User can sign in",
        });
      } catch (e) {
        console.log(e);
        return done(null, false, {
          message: "User can't sign in",
        });
      }
    }
  )
);

exports.user = (req, res, next) => {
  // It will go to ../middlewares/auth/index.js -> passport.use("signup")
  passport.authorize("user", (err, user, info) => {

    // If error
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err.message,
      });
    }

    // If user is false
    if (!user) {
      return res.status(403).json({
        message: info.message,
      });
    }

    req.user = user;

    // Next to authController.getToken
    next();
  })(req, res, next);
};

passport.use(
  "user",
  new JWTstrategy(
    {
      secretOrKey: process.env.JWT_SECRET, // JWT Key
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(), // Get token from bearer
    },
    async (token, done) => {
      try {
        // Find user
        console.log(token);
        const userLogin = await user.findOne({ id: token.user.id });

        // If user exist
        if (userLogin) {
          return done(null, token.user);
        }

        return done(null, false, {
          message: "You're not authorized",
        });
      } catch (e) {
        return done(null, false, {
          message: "You're not authorized",
        });
      }
    }
  )
);
