const {
    user,
    account,
    transaction
} = require('../models')
const Sequelize = require('sequelize');
const db = require('../models/index');
class TransactionController {
    // Get all
    async get(req, res) {
        try {
            let data = await transaction.findAll({where:{user_id:req.user.id,account_id:req.params.account_id}});
            return res.status(200).json({
                message: "Success",
                data,
            });
        } catch (e) {
            console.log(e);
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }

    async delete(req, res) {
        try {
            await transaction.destroy({
                where: {
                    id: req.params.id
                }
            });
            return res.status(201).json({
                message: "Success",
            });
        } catch (e) {
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }

    async debit(req, res) {
        const t = await db.sequelize.transaction();
        try {
            
           const createData= await transaction.create({
               user_id:req.user.id,
               currency:req.body.currency,
               debit:parseFloat(req.body.debit),
               description:req.body.description
           }, {
                transaction: t
            });
            await account.increment('balance' ,{by:parseFloat(req.body.debit),
                where: {
                    id: req.params.account_id
                }
            }, {
                transaction: t
            });
            await t.commit();
            return res.status(201).json({
                message: "Success",
                data:createData
            });
        } catch (e) {
            await t.rollback();
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }
    async credit(req, res) {
        const t = await db.sequelize.transaction();
        try {
            
           const createData= await transaction.create({
               user_id:req.user.id,
               currency:req.body.currency,
               credit:parseFloat(req.body.credit),
               description:req.body.description
           }, {
                transaction: t
            });
            await account.decrement('balance' ,{by:parseFloat(req.body.debit),
                where: {
                    id: req.params.account_id
                }
            }, {
                transaction: t
            });
            await t.commit();
            return res.status(201).json({
                message: "Success",
                data:createData
            });
        } catch (e) {
            await t.rollback();
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }
}

module.exports = new TransactionController();