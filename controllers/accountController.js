const {
    user,
    account,
    transaction
} = require('../models')
const Sequelize = require('sequelize');

const makeid = (length) => {
    var result = [];
    var characters = "0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }
    return result.join('');
}


class AccountController {
    async get(req, res) {
        try {
            const data = await account.findOne({
                where: {
                    id: req.user.id
                },

            })

            // If success
            return res.status(200).json({
                message: "Success",
                data,
            });
        } catch (e) {
            console.log(e);
            // If error
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }

    // Create account
    async create(req, res) {
        try {
            let createdData = await account.create({
                user_id: parseInt(req.user.id),
                account_number: makeid(20),
                currency: req.body.currency,
                balance: parseFloat(req.body.balance),
                last_transaction_date: new Date()
            });
            const data = await account.findOne({
                where: {
                    id: createdData.id
                },
                attributes: ["id", "account_number", "currency", "balance", "last_transaction_date"],
                include: [
                  //join
                  { model: user, attributes: ["name","email"] },
                ],
        
            });

            return res.status(201).json({
                message: "Success",
                data: data,
            });
        } catch (e) {
            console.log(e);
            return res.status(500).json({
                message: "Internal Server Error",
                error: e.message,
            });
        }
    }

}

module.exports = new AccountController();