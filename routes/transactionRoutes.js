const express = require("express"); // Import express

// Import controller
const transactionController = require("../controllers/transactionController");

// Import auth (middleware)
const auth = require("../middlewares/auth");

// Make router
const router = express.Router();

// If user access /transaction
router.get(
  "/:account_id",auth.user,
  transactionController.get
);

// If user access /transaction
router.post(
    "/debit/:account_id",auth.user,
    transactionController.debit
  );

  // If user access /transaction
// router.post(
//     "/credit",auth.user,
//     transactionController.credit
//   );

module.exports = router;
