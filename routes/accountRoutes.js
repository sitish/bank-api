const express = require("express"); // Import express

// Import controller
const accountController = require("../controllers/accountController");

// Import auth (middleware)
const auth = require("../middlewares/auth");

// Make router
const router = express.Router();

// If user access /account
router.get(
  "/",auth.user,
  accountController.get
);

// If user access /account
router.post(
    "/",auth.user,
    accountController.create
  );

module.exports = router;
